﻿Option Strict On
Option Explicit On
Imports System.IO
Public Class mainGame
    Public solidStateHands() As String = {"Two sets of Three", "One set of three, One run of three", "One set of four, One run of four" _
                                  , "One run of seven", "One run of Eight", "One run of nine", "Two sets of four", "Seven cards of one color", _
                                   "One set of five, One set of Two", "One set of five, one set of Three"}

    Public currentTurn As Integer = 1

    Public scores() As Integer = {0, 0, 0, 0}
    Public playerTurns() As Integer = {1, 1, 1, 1}
    Public permenantScoresFromFile(3) As Integer
    Public handReqs() As String = solidStateHands
    Public funReqs() As String = {"One set of five, One purple card", "Two sets of four, One red card", "One run of five, One set of four", _
                                  "One run of Ten", "One run of six, One set of three - Last card played to win must be on another players phase sets."}
    Private Sub mainGame_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        updateMainTurnCounter()
        updateScoreToListBoxes()
        updatePlayerTurns()
        onGoingSaveFileLoad()
        updateOnGoingScores()
    End Sub
    Private Sub updateMainTurnCounter()
        TurnCounter.Text = currentTurn.ToString
    End Sub
    Private Sub CloseButton_Click(sender As Object, e As EventArgs) Handles CloseButton.Click
        updateOnGoingSaveFile()
        Close()
    End Sub
    Private Sub ResetButton_Click(sender As Object, e As EventArgs) Handles ResetButton.Click
        Dim secondOpinionSwitch As Boolean = False

        MsgBox("You are about to reset scores. Are you sure you want to do this?", CType(1, MsgBoxStyle), "Are you sure?")
        If MsgBoxResult.Ok = 1 Then
            MsgBox("Ok")
        ElseIf MsgBoxResult.Cancel = 1 Then
            MsgBox("Cancel")
        End If
        clearAllFields()
        currentTurn = 1
        updateMainTurnCounter()
        updatePlayerTurns()
        ReDim scores(3)
        clearTurnLabels()
        clearAllColors()
        SaveTextBox.Text = String.Empty
    End Sub
    Private Sub clearAllFields()
        clearTextBoxes()
        clearCheckBoxes()
        MListBox.Items.Clear()
        JListBox.Items.Clear()
        AListBox.Items.Clear()
        CListBox.Items.Clear()
    End Sub
    Private Sub clearAllColors()
        M.BackColor = Color.Transparent
        J.BackColor = Color.Transparent
        A.BackColor = Color.Transparent
        C.BackColor = Color.Transparent
    End Sub
    Private Sub clearTextBoxes()
        MTextBox.Text = String.Empty
        JTextBox.Text = String.Empty
        ATextBox.Text = String.Empty
        CTextBox.Text = String.Empty
    End Sub
    Private Sub updateScoreToListBoxes()
        MListBox.Items.Add(scores(0))
        JListBox.Items.Add(scores(1))
        AListBox.Items.Add(scores(2))
        CListBox.Items.Add(scores(3))
    End Sub
    Private Sub EndTurnButton_Click(sender As Object, e As EventArgs) Handles EndTurnButton.Click
        Dim currentScores(3) As Integer
        Dim winner As String = "X"
        Dim loser As String = "X"

        Try
            If JTextBox.Text = String.Empty Then
                currentScores(1) = 0
            Else
                currentScores(1) = Convert.ToInt32(JTextBox.Text)
            End If
            If MTextBox.Text = String.Empty Then
                currentScores(0) = 0
            Else
                currentScores(0) = Convert.ToInt32(MTextBox.Text)
            End If
            If ATextBox.Text = String.Empty Then
                currentScores(2) = 0
            Else
                currentScores(2) = Convert.ToInt32(ATextBox.Text)
            End If
            If CTextBox.Text = String.Empty Then
                currentScores(3) = 0
            Else
                currentScores(3) = Convert.ToInt32(CTextBox.Text)
            End If

            currentTurn += 1
            checkBoxesForChecks()
            calculateNewScores(currentScores)
            clearTextBoxes()
            clearCheckBoxes()
            updateScoreToListBoxes()


            calculatePermenantSaveFile()
            updateOnGoingScores()
            updateOnGoingSaveFile()


            updatePlayerTurns()
            updateMainTurnCounter()
            findCurrentLoser(loser)
            findCurrentWinner(winner)
            colorChanger(winner, loser)
            If SaveTextBox.Text = "" Then
                MsgBox("You did not enter in a save path this round. No autosave completed!")
            Else
                autoSave()
            End If
        Catch ex As Exception
            MsgBox("Please enter numbers only starting with 0 into the score boxes.")
        End Try


    End Sub
    Public Sub updateOnGoingScores()
        MOnGoingLabel.Text = permenantScoresFromFile(0).ToString
        JOnGoingLabel.Text = permenantScoresFromFile(1).ToString
        AOnGoingLabel.Text = permenantScoresFromFile(2).ToString
        COnGoingLabel.Text = permenantScoresFromFile(3).ToString
    End Sub
    Private Sub calculateNewScores(ByVal currentScores() As Integer)
        scores(0) = scores(0) + currentScores(0)
        scores(1) = scores(1) + currentScores(1)
        scores(2) = scores(2) + currentScores(2)
        scores(3) = scores(3) + currentScores(3)
    End Sub
    Private Sub findCurrentWinner(ByRef winner As String)
        If scores(0) < scores(1) And scores(0) < scores(2) And scores(0) < scores(3) Then
            winner = "M"
        ElseIf scores(1) < scores(0) And scores(1) < scores(2) And scores(1) < scores(3) Then
            winner = "J"
        ElseIf scores(2) < scores(0) And scores(2) < scores(1) And scores(2) < scores(3) Then
            winner = "A"
        ElseIf scores(3) < scores(0) And scores(3) < scores(1) And scores(3) < scores(2) Then
            winner = "C"
        End If

    End Sub
    Private Sub findCurrentLoser(ByRef loser As String)
        If scores(0) > scores(1) And scores(0) > scores(2) And scores(0) > scores(3) Then
            loser = "M"
        ElseIf scores(1) > scores(0) And scores(1) > scores(2) And scores(1) > scores(3) Then
            loser = "J"
        ElseIf scores(2) > scores(0) And scores(2) > scores(1) And scores(2) > scores(3) Then
            loser = "A"
        ElseIf scores(3) > scores(0) And scores(3) > scores(1) And scores(3) > scores(2) Then
            loser = "C"
        End If
    End Sub
    Private Sub colorChanger(ByVal winner As String, ByRef loser As String)

        Dim x() As Integer = {0, 0, 0, 0}

        If winner = "M" Then
            M.BackColor = Color.Blue
            x(0) = 1
        ElseIf winner = "J" Then
            J.BackColor = Color.Blue
            x(1) = 1
        ElseIf winner = "A" Then
            A.BackColor = Color.Blue
            x(2) = 1
        ElseIf winner = "C" Then
            C.BackColor = Color.Blue
            x(3) = 1
        End If

        If loser = "M" Then
            M.BackColor = Color.Red
            x(0) = 1
        ElseIf loser = "J" Then
            J.BackColor = Color.Red
            x(1) = 1
        ElseIf loser = "A" Then
            A.BackColor = Color.Red
            x(2) = 1
        ElseIf loser = "C" Then
            C.BackColor = Color.Red
            x(3) = 1
        End If

        For i As Integer = 0 To 3
            If x(0) = 0 Then
                M.BackColor = Color.Transparent
                x(0) = 1
            ElseIf x(1) = 0 Then
                J.BackColor = Color.Transparent
                x(1) = 1
            ElseIf x(2) = 0 Then
                A.BackColor = Color.Transparent
                x(2) = 1
            ElseIf x(3) = 0 Then
                C.BackColor = Color.Transparent
                x(3) = 1
            End If
        Next


    End Sub
    Private Sub checkBoxesForChecks()
        If MCheckBox.Checked = True Then
            playerTurns(0) += 1
        End If
        If JCheckBox.Checked = True Then
            playerTurns(1) += 1
        End If
        If ACheckBox.Checked = True Then
            playerTurns(2) += 1
        End If
        If CCheckBox.Checked = True Then
            playerTurns(3) += 1
        End If

    End Sub
    Private Sub clearCheckBoxes()
        MCheckBox.Checked = False
        JCheckBox.Checked = False
        ACheckBox.Checked = False
        CCheckBox.Checked = False
    End Sub
    Private Sub updatePlayerTurns()
        MTurnLabel.Text = playerTurns(0).ToString
        JTurnLabel.Text = playerTurns(1).ToString
        ATurnLabel.Text = playerTurns(2).ToString
        CTurnLabel.Text = playerTurns(3).ToString
    End Sub
    Private Sub clearTurnLabels()
        playerTurns(0) = 1
        playerTurns(1) = 1
        playerTurns(2) = 1
        playerTurns(3) = 1
        updatePlayerTurns()
    End Sub
    Private Sub CheckPhaseButton_Click(sender As Object, e As EventArgs) Handles CheckPhaseButton.Click
        Try
            Dim phaseInput As Integer = Convert.ToInt32(PhaseTextBox.Text)
            MsgBox(handReqs(phaseInput - 1))
        Catch ex As Exception
            MsgBox("Please enter a numbers into the check phase box.    (Will not accept empty space either)")
        End Try

        PhaseTextBox.Text = String.Empty
    End Sub
    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        SaveFileDialog.ShowDialog()
    End Sub
    Private Sub autoSave()
        Dim userSaveFile As String = SaveFileDialog.FileName

        FileOpen(1, userSaveFile, OpenMode.Output)
        PrintLine(1, "The turn was " & currentTurn)
        PrintLine(1, "M - Score: " & scores(0) & ", On Phase: " & playerTurns(0))
        PrintLine(1, "J - Score: " & scores(1) & ", On Phase: " & playerTurns(1))
        PrintLine(1, "A - Score: " & scores(2) & ", On Phase: " & playerTurns(2))
        PrintLine(1, "C - Score: " & scores(3) & ", On Phase: " & playerTurns(3))
        FileClose(1)



    End Sub
    Public Sub updateOnGoingSaveFile()
        FileOpen(3, "ongoingScores.txt", OpenMode.Output)
        PrintLine(3, permenantScoresFromFile(0).ToString)
        PrintLine(3, permenantScoresFromFile(1).ToString)
        PrintLine(3, permenantScoresFromFile(2).ToString)
        PrintLine(3, permenantScoresFromFile(3).ToString)
        FileClose(3)
    End Sub
    Public Sub calculatePermenantSaveFile()
        permenantScoresFromFile(0) += scores(0)
        permenantScoresFromFile(1) += scores(1)
        permenantScoresFromFile(2) += scores(2)
        permenantScoresFromFile(3) += scores(3)
    End Sub
    Public Sub onGoingSaveFileLoad()
        FileOpen(2, "ongoingScores.txt", OpenMode.Input)
        Input(2, permenantScoresFromFile(0))
        Input(2, permenantScoresFromFile(1))
        Input(2, permenantScoresFromFile(2))
        Input(2, permenantScoresFromFile(3))

        FileClose(2)
    End Sub
    Private Sub SaveFileDialog_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog.FileOk
        SaveTextBox.Text = SaveFileDialog.FileName
    End Sub
    Private Sub FunReqsCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles FunReqsCheckBox.CheckedChanged
        If FunReqsCheckBox.Checked = True Then
            ReDim handReqs(4)
            For i As Integer = 0 To 4
                handReqs(i) = funReqs(i)
            Next
        ElseIf FunReqsCheckBox.Checked = False Then
            ReDim handReqs(9)
            For i As Integer = 0 To 9
                handReqs(i) = solidStateHands(i)
            Next
        End If
    End Sub
End Class
