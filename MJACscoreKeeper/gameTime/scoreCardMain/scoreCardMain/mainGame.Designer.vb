﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CloseButton = New System.Windows.Forms.Button()
        Me.EndTurnButton = New System.Windows.Forms.Button()
        Me.ResetButton = New System.Windows.Forms.Button()
        Me.M = New System.Windows.Forms.Label()
        Me.J = New System.Windows.Forms.Label()
        Me.A = New System.Windows.Forms.Label()
        Me.C = New System.Windows.Forms.Label()
        Me.MainGroupBox = New System.Windows.Forms.GroupBox()
        Me.BodySplitContainer = New System.Windows.Forms.SplitContainer()
        Me.LeftSplitContainer = New System.Windows.Forms.SplitContainer()
        Me.MListBox = New System.Windows.Forms.ListBox()
        Me.JListBox = New System.Windows.Forms.ListBox()
        Me.RightSplitContainer = New System.Windows.Forms.SplitContainer()
        Me.AListBox = New System.Windows.Forms.ListBox()
        Me.CListBox = New System.Windows.Forms.ListBox()
        Me.MTextBox = New System.Windows.Forms.TextBox()
        Me.JTextBox = New System.Windows.Forms.TextBox()
        Me.ATextBox = New System.Windows.Forms.TextBox()
        Me.CTextBox = New System.Windows.Forms.TextBox()
        Me.TurnCounter = New System.Windows.Forms.Label()
        Me.MCheckBox = New System.Windows.Forms.CheckBox()
        Me.JCheckBox = New System.Windows.Forms.CheckBox()
        Me.ACheckBox = New System.Windows.Forms.CheckBox()
        Me.CCheckBox = New System.Windows.Forms.CheckBox()
        Me.MTurnLabel = New System.Windows.Forms.Label()
        Me.JTurnLabel = New System.Windows.Forms.Label()
        Me.ATurnLabel = New System.Windows.Forms.Label()
        Me.CTurnLabel = New System.Windows.Forms.Label()
        Me.TurnCounterLabel = New System.Windows.Forms.Label()
        Me.PhaseTextBox = New System.Windows.Forms.TextBox()
        Me.PhaseLabel = New System.Windows.Forms.Label()
        Me.CheckPhaseButton = New System.Windows.Forms.Button()
        Me.SaveFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.SaveTextBox = New System.Windows.Forms.TextBox()
        Me.FunReqsCheckBox = New System.Windows.Forms.CheckBox()
        Me.MOnGoingLabel = New System.Windows.Forms.Label()
        Me.JOnGoingLabel = New System.Windows.Forms.Label()
        Me.AOnGoingLabel = New System.Windows.Forms.Label()
        Me.COnGoingLabel = New System.Windows.Forms.Label()
        Me.PictureBox = New System.Windows.Forms.PictureBox()
        Me.MainGroupBox.SuspendLayout()
        Me.BodySplitContainer.Panel1.SuspendLayout()
        Me.BodySplitContainer.Panel2.SuspendLayout()
        Me.BodySplitContainer.SuspendLayout()
        Me.LeftSplitContainer.Panel1.SuspendLayout()
        Me.LeftSplitContainer.Panel2.SuspendLayout()
        Me.LeftSplitContainer.SuspendLayout()
        Me.RightSplitContainer.Panel1.SuspendLayout()
        Me.RightSplitContainer.Panel2.SuspendLayout()
        Me.RightSplitContainer.SuspendLayout()
        CType(Me.PictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CloseButton
        '
        Me.CloseButton.BackColor = System.Drawing.Color.Yellow
        Me.CloseButton.Location = New System.Drawing.Point(230, 416)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(75, 23)
        Me.CloseButton.TabIndex = 0
        Me.CloseButton.Text = "Close"
        Me.CloseButton.UseVisualStyleBackColor = False
        '
        'EndTurnButton
        '
        Me.EndTurnButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.EndTurnButton.Location = New System.Drawing.Point(149, 416)
        Me.EndTurnButton.Name = "EndTurnButton"
        Me.EndTurnButton.Size = New System.Drawing.Size(75, 23)
        Me.EndTurnButton.TabIndex = 1
        Me.EndTurnButton.Text = "End Turn"
        Me.EndTurnButton.UseVisualStyleBackColor = False
        '
        'ResetButton
        '
        Me.ResetButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ResetButton.Location = New System.Drawing.Point(13, 416)
        Me.ResetButton.Name = "ResetButton"
        Me.ResetButton.Size = New System.Drawing.Size(130, 23)
        Me.ResetButton.TabIndex = 2
        Me.ResetButton.Text = "Reset"
        Me.ResetButton.UseVisualStyleBackColor = False
        '
        'M
        '
        Me.M.AutoSize = True
        Me.M.BackColor = System.Drawing.Color.DodgerBlue
        Me.M.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M.Location = New System.Drawing.Point(12, 50)
        Me.M.Name = "M"
        Me.M.Size = New System.Drawing.Size(30, 26)
        Me.M.TabIndex = 3
        Me.M.Text = "M"
        '
        'J
        '
        Me.J.AutoSize = True
        Me.J.BackColor = System.Drawing.Color.Gold
        Me.J.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.J.Location = New System.Drawing.Point(93, 50)
        Me.J.Name = "J"
        Me.J.Size = New System.Drawing.Size(23, 26)
        Me.J.TabIndex = 4
        Me.J.Text = "J"
        '
        'A
        '
        Me.A.AutoSize = True
        Me.A.BackColor = System.Drawing.Color.LimeGreen
        Me.A.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.A.Location = New System.Drawing.Point(167, 49)
        Me.A.Name = "A"
        Me.A.Size = New System.Drawing.Size(27, 26)
        Me.A.TabIndex = 5
        Me.A.Text = "A"
        '
        'C
        '
        Me.C.AutoSize = True
        Me.C.BackColor = System.Drawing.Color.Red
        Me.C.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C.Location = New System.Drawing.Point(237, 49)
        Me.C.Name = "C"
        Me.C.Size = New System.Drawing.Size(28, 26)
        Me.C.TabIndex = 6
        Me.C.Text = "C"
        '
        'MainGroupBox
        '
        Me.MainGroupBox.BackColor = System.Drawing.Color.LimeGreen
        Me.MainGroupBox.Controls.Add(Me.BodySplitContainer)
        Me.MainGroupBox.Location = New System.Drawing.Point(7, 121)
        Me.MainGroupBox.Name = "MainGroupBox"
        Me.MainGroupBox.Size = New System.Drawing.Size(303, 292)
        Me.MainGroupBox.TabIndex = 7
        Me.MainGroupBox.TabStop = False
        Me.MainGroupBox.Text = "Scores"
        '
        'BodySplitContainer
        '
        Me.BodySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BodySplitContainer.Location = New System.Drawing.Point(3, 16)
        Me.BodySplitContainer.Name = "BodySplitContainer"
        '
        'BodySplitContainer.Panel1
        '
        Me.BodySplitContainer.Panel1.Controls.Add(Me.LeftSplitContainer)
        '
        'BodySplitContainer.Panel2
        '
        Me.BodySplitContainer.Panel2.Controls.Add(Me.RightSplitContainer)
        Me.BodySplitContainer.Size = New System.Drawing.Size(297, 273)
        Me.BodySplitContainer.SplitterDistance = 150
        Me.BodySplitContainer.TabIndex = 0
        '
        'LeftSplitContainer
        '
        Me.LeftSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LeftSplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.LeftSplitContainer.Name = "LeftSplitContainer"
        '
        'LeftSplitContainer.Panel1
        '
        Me.LeftSplitContainer.Panel1.Controls.Add(Me.MListBox)
        '
        'LeftSplitContainer.Panel2
        '
        Me.LeftSplitContainer.Panel2.Controls.Add(Me.JListBox)
        Me.LeftSplitContainer.Size = New System.Drawing.Size(150, 273)
        Me.LeftSplitContainer.SplitterDistance = 72
        Me.LeftSplitContainer.TabIndex = 0
        '
        'MListBox
        '
        Me.MListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MListBox.FormattingEnabled = True
        Me.MListBox.Location = New System.Drawing.Point(0, 0)
        Me.MListBox.Name = "MListBox"
        Me.MListBox.Size = New System.Drawing.Size(72, 273)
        Me.MListBox.TabIndex = 0
        '
        'JListBox
        '
        Me.JListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.JListBox.FormattingEnabled = True
        Me.JListBox.Location = New System.Drawing.Point(0, 0)
        Me.JListBox.Name = "JListBox"
        Me.JListBox.Size = New System.Drawing.Size(74, 273)
        Me.JListBox.TabIndex = 0
        '
        'RightSplitContainer
        '
        Me.RightSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RightSplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.RightSplitContainer.Name = "RightSplitContainer"
        '
        'RightSplitContainer.Panel1
        '
        Me.RightSplitContainer.Panel1.Controls.Add(Me.AListBox)
        '
        'RightSplitContainer.Panel2
        '
        Me.RightSplitContainer.Panel2.Controls.Add(Me.CListBox)
        Me.RightSplitContainer.Size = New System.Drawing.Size(143, 273)
        Me.RightSplitContainer.SplitterDistance = 73
        Me.RightSplitContainer.TabIndex = 0
        '
        'AListBox
        '
        Me.AListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AListBox.FormattingEnabled = True
        Me.AListBox.Location = New System.Drawing.Point(0, 0)
        Me.AListBox.Name = "AListBox"
        Me.AListBox.Size = New System.Drawing.Size(73, 273)
        Me.AListBox.TabIndex = 0
        '
        'CListBox
        '
        Me.CListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CListBox.FormattingEnabled = True
        Me.CListBox.Location = New System.Drawing.Point(0, 0)
        Me.CListBox.Name = "CListBox"
        Me.CListBox.Size = New System.Drawing.Size(66, 273)
        Me.CListBox.TabIndex = 0
        '
        'MTextBox
        '
        Me.MTextBox.Location = New System.Drawing.Point(7, 79)
        Me.MTextBox.Name = "MTextBox"
        Me.MTextBox.Size = New System.Drawing.Size(75, 20)
        Me.MTextBox.TabIndex = 8
        '
        'JTextBox
        '
        Me.JTextBox.Location = New System.Drawing.Point(83, 79)
        Me.JTextBox.Name = "JTextBox"
        Me.JTextBox.Size = New System.Drawing.Size(75, 20)
        Me.JTextBox.TabIndex = 9
        '
        'ATextBox
        '
        Me.ATextBox.Location = New System.Drawing.Point(159, 79)
        Me.ATextBox.Name = "ATextBox"
        Me.ATextBox.Size = New System.Drawing.Size(75, 20)
        Me.ATextBox.TabIndex = 10
        '
        'CTextBox
        '
        Me.CTextBox.Location = New System.Drawing.Point(235, 79)
        Me.CTextBox.Name = "CTextBox"
        Me.CTextBox.Size = New System.Drawing.Size(75, 20)
        Me.CTextBox.TabIndex = 11
        '
        'TurnCounter
        '
        Me.TurnCounter.AutoSize = True
        Me.TurnCounter.BackColor = System.Drawing.Color.Transparent
        Me.TurnCounter.Font = New System.Drawing.Font("Monotype Corsiva", 27.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TurnCounter.Location = New System.Drawing.Point(225, 2)
        Me.TurnCounter.Name = "TurnCounter"
        Me.TurnCounter.Size = New System.Drawing.Size(41, 44)
        Me.TurnCounter.TabIndex = 12
        Me.TurnCounter.Text = "X"
        '
        'MCheckBox
        '
        Me.MCheckBox.AutoSize = True
        Me.MCheckBox.BackColor = System.Drawing.Color.Transparent
        Me.MCheckBox.Location = New System.Drawing.Point(17, 103)
        Me.MCheckBox.Name = "MCheckBox"
        Me.MCheckBox.Size = New System.Drawing.Size(54, 17)
        Me.MCheckBox.TabIndex = 13
        Me.MCheckBox.Text = "Down"
        Me.MCheckBox.UseVisualStyleBackColor = False
        '
        'JCheckBox
        '
        Me.JCheckBox.AutoSize = True
        Me.JCheckBox.BackColor = System.Drawing.Color.Transparent
        Me.JCheckBox.Location = New System.Drawing.Point(97, 103)
        Me.JCheckBox.Name = "JCheckBox"
        Me.JCheckBox.Size = New System.Drawing.Size(54, 17)
        Me.JCheckBox.TabIndex = 14
        Me.JCheckBox.Text = "Down"
        Me.JCheckBox.UseVisualStyleBackColor = False
        '
        'ACheckBox
        '
        Me.ACheckBox.AutoSize = True
        Me.ACheckBox.BackColor = System.Drawing.Color.Yellow
        Me.ACheckBox.Location = New System.Drawing.Point(167, 103)
        Me.ACheckBox.Name = "ACheckBox"
        Me.ACheckBox.Size = New System.Drawing.Size(54, 17)
        Me.ACheckBox.TabIndex = 15
        Me.ACheckBox.Text = "Down"
        Me.ACheckBox.UseVisualStyleBackColor = False
        '
        'CCheckBox
        '
        Me.CCheckBox.AutoSize = True
        Me.CCheckBox.BackColor = System.Drawing.Color.Lime
        Me.CCheckBox.Location = New System.Drawing.Point(242, 103)
        Me.CCheckBox.Name = "CCheckBox"
        Me.CCheckBox.Size = New System.Drawing.Size(54, 17)
        Me.CCheckBox.TabIndex = 16
        Me.CCheckBox.Text = "Down"
        Me.CCheckBox.UseVisualStyleBackColor = False
        '
        'MTurnLabel
        '
        Me.MTurnLabel.AutoSize = True
        Me.MTurnLabel.BackColor = System.Drawing.Color.Yellow
        Me.MTurnLabel.Location = New System.Drawing.Point(47, 62)
        Me.MTurnLabel.Name = "MTurnLabel"
        Me.MTurnLabel.Size = New System.Drawing.Size(23, 13)
        Me.MTurnLabel.TabIndex = 17
        Me.MTurnLabel.Text = "MX"
        '
        'JTurnLabel
        '
        Me.JTurnLabel.AutoSize = True
        Me.JTurnLabel.BackColor = System.Drawing.Color.Yellow
        Me.JTurnLabel.Location = New System.Drawing.Point(122, 63)
        Me.JTurnLabel.Name = "JTurnLabel"
        Me.JTurnLabel.Size = New System.Drawing.Size(19, 13)
        Me.JTurnLabel.TabIndex = 18
        Me.JTurnLabel.Text = "JX"
        '
        'ATurnLabel
        '
        Me.ATurnLabel.AutoSize = True
        Me.ATurnLabel.BackColor = System.Drawing.Color.Yellow
        Me.ATurnLabel.Location = New System.Drawing.Point(200, 63)
        Me.ATurnLabel.Name = "ATurnLabel"
        Me.ATurnLabel.Size = New System.Drawing.Size(21, 13)
        Me.ATurnLabel.TabIndex = 19
        Me.ATurnLabel.Text = "AX"
        '
        'CTurnLabel
        '
        Me.CTurnLabel.AutoSize = True
        Me.CTurnLabel.BackColor = System.Drawing.Color.Yellow
        Me.CTurnLabel.Location = New System.Drawing.Point(271, 62)
        Me.CTurnLabel.Name = "CTurnLabel"
        Me.CTurnLabel.Size = New System.Drawing.Size(21, 13)
        Me.CTurnLabel.TabIndex = 20
        Me.CTurnLabel.Text = "CX"
        '
        'TurnCounterLabel
        '
        Me.TurnCounterLabel.AutoSize = True
        Me.TurnCounterLabel.BackColor = System.Drawing.Color.Transparent
        Me.TurnCounterLabel.Font = New System.Drawing.Font("Monotype Corsiva", 27.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TurnCounterLabel.Location = New System.Drawing.Point(145, 2)
        Me.TurnCounterLabel.Name = "TurnCounterLabel"
        Me.TurnCounterLabel.Size = New System.Drawing.Size(87, 44)
        Me.TurnCounterLabel.TabIndex = 21
        Me.TurnCounterLabel.Text = "Turn"
        '
        'PhaseTextBox
        '
        Me.PhaseTextBox.Location = New System.Drawing.Point(170, 450)
        Me.PhaseTextBox.Name = "PhaseTextBox"
        Me.PhaseTextBox.Size = New System.Drawing.Size(32, 20)
        Me.PhaseTextBox.TabIndex = 22
        '
        'PhaseLabel
        '
        Me.PhaseLabel.AutoSize = True
        Me.PhaseLabel.BackColor = System.Drawing.Color.Transparent
        Me.PhaseLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PhaseLabel.Location = New System.Drawing.Point(103, 452)
        Me.PhaseLabel.Name = "PhaseLabel"
        Me.PhaseLabel.Size = New System.Drawing.Size(66, 17)
        Me.PhaseLabel.TabIndex = 23
        Me.PhaseLabel.Text = "Phases:"
        '
        'CheckPhaseButton
        '
        Me.CheckPhaseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.CheckPhaseButton.Location = New System.Drawing.Point(209, 448)
        Me.CheckPhaseButton.Name = "CheckPhaseButton"
        Me.CheckPhaseButton.Size = New System.Drawing.Size(101, 23)
        Me.CheckPhaseButton.TabIndex = 24
        Me.CheckPhaseButton.Text = "Check Phase"
        Me.CheckPhaseButton.UseVisualStyleBackColor = False
        '
        'SaveFileDialog
        '
        Me.SaveFileDialog.Filter = "(*.txt) | *.txt"
        '
        'SaveButton
        '
        Me.SaveButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.SaveButton.Location = New System.Drawing.Point(18, 483)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(75, 23)
        Me.SaveButton.TabIndex = 25
        Me.SaveButton.Text = "Save As..."
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'SaveTextBox
        '
        Me.SaveTextBox.Location = New System.Drawing.Point(94, 484)
        Me.SaveTextBox.Name = "SaveTextBox"
        Me.SaveTextBox.Size = New System.Drawing.Size(207, 20)
        Me.SaveTextBox.TabIndex = 26
        '
        'FunReqsCheckBox
        '
        Me.FunReqsCheckBox.AutoSize = True
        Me.FunReqsCheckBox.BackColor = System.Drawing.Color.Transparent
        Me.FunReqsCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FunReqsCheckBox.Location = New System.Drawing.Point(10, 452)
        Me.FunReqsCheckBox.Name = "FunReqsCheckBox"
        Me.FunReqsCheckBox.Size = New System.Drawing.Size(79, 21)
        Me.FunReqsCheckBox.TabIndex = 27
        Me.FunReqsCheckBox.Text = "Phase 5"
        Me.FunReqsCheckBox.UseVisualStyleBackColor = False
        '
        'MOnGoingLabel
        '
        Me.MOnGoingLabel.AutoSize = True
        Me.MOnGoingLabel.BackColor = System.Drawing.Color.Transparent
        Me.MOnGoingLabel.Location = New System.Drawing.Point(47, 49)
        Me.MOnGoingLabel.Name = "MOnGoingLabel"
        Me.MOnGoingLabel.Size = New System.Drawing.Size(30, 13)
        Me.MOnGoingLabel.TabIndex = 28
        Me.MOnGoingLabel.Text = "MXX"
        '
        'JOnGoingLabel
        '
        Me.JOnGoingLabel.AutoSize = True
        Me.JOnGoingLabel.BackColor = System.Drawing.Color.Transparent
        Me.JOnGoingLabel.Location = New System.Drawing.Point(122, 49)
        Me.JOnGoingLabel.Name = "JOnGoingLabel"
        Me.JOnGoingLabel.Size = New System.Drawing.Size(30, 13)
        Me.JOnGoingLabel.TabIndex = 29
        Me.JOnGoingLabel.Text = "MXX"
        '
        'AOnGoingLabel
        '
        Me.AOnGoingLabel.AutoSize = True
        Me.AOnGoingLabel.BackColor = System.Drawing.Color.Transparent
        Me.AOnGoingLabel.Location = New System.Drawing.Point(200, 50)
        Me.AOnGoingLabel.Name = "AOnGoingLabel"
        Me.AOnGoingLabel.Size = New System.Drawing.Size(30, 13)
        Me.AOnGoingLabel.TabIndex = 30
        Me.AOnGoingLabel.Text = "MXX"
        '
        'COnGoingLabel
        '
        Me.COnGoingLabel.AutoSize = True
        Me.COnGoingLabel.BackColor = System.Drawing.Color.Transparent
        Me.COnGoingLabel.Location = New System.Drawing.Point(271, 49)
        Me.COnGoingLabel.Name = "COnGoingLabel"
        Me.COnGoingLabel.Size = New System.Drawing.Size(30, 13)
        Me.COnGoingLabel.TabIndex = 31
        Me.COnGoingLabel.Text = "MXX"
        '
        'PictureBox
        '
        Me.PictureBox.BackgroundImage = Global.scoreCardMain.My.Resources.Resources.logo
        Me.PictureBox.Location = New System.Drawing.Point(36, 2)
        Me.PictureBox.Name = "PictureBox"
        Me.PictureBox.Size = New System.Drawing.Size(80, 42)
        Me.PictureBox.TabIndex = 32
        Me.PictureBox.TabStop = False
        '
        'mainGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.scoreCardMain.My.Resources.Resources.sma
        Me.ClientSize = New System.Drawing.Size(318, 515)
        Me.Controls.Add(Me.PictureBox)
        Me.Controls.Add(Me.COnGoingLabel)
        Me.Controls.Add(Me.AOnGoingLabel)
        Me.Controls.Add(Me.JOnGoingLabel)
        Me.Controls.Add(Me.MOnGoingLabel)
        Me.Controls.Add(Me.FunReqsCheckBox)
        Me.Controls.Add(Me.SaveTextBox)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.CheckPhaseButton)
        Me.Controls.Add(Me.PhaseLabel)
        Me.Controls.Add(Me.PhaseTextBox)
        Me.Controls.Add(Me.TurnCounterLabel)
        Me.Controls.Add(Me.CTurnLabel)
        Me.Controls.Add(Me.ATurnLabel)
        Me.Controls.Add(Me.JTurnLabel)
        Me.Controls.Add(Me.MTurnLabel)
        Me.Controls.Add(Me.CCheckBox)
        Me.Controls.Add(Me.ACheckBox)
        Me.Controls.Add(Me.JCheckBox)
        Me.Controls.Add(Me.MCheckBox)
        Me.Controls.Add(Me.TurnCounter)
        Me.Controls.Add(Me.CTextBox)
        Me.Controls.Add(Me.ATextBox)
        Me.Controls.Add(Me.JTextBox)
        Me.Controls.Add(Me.MTextBox)
        Me.Controls.Add(Me.MainGroupBox)
        Me.Controls.Add(Me.C)
        Me.Controls.Add(Me.A)
        Me.Controls.Add(Me.J)
        Me.Controls.Add(Me.M)
        Me.Controls.Add(Me.ResetButton)
        Me.Controls.Add(Me.EndTurnButton)
        Me.Controls.Add(Me.CloseButton)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(334, 553)
        Me.MinimumSize = New System.Drawing.Size(334, 553)
        Me.Name = "mainGame"
        Me.Text = "MJAC"
        Me.MainGroupBox.ResumeLayout(False)
        Me.BodySplitContainer.Panel1.ResumeLayout(False)
        Me.BodySplitContainer.Panel2.ResumeLayout(False)
        Me.BodySplitContainer.ResumeLayout(False)
        Me.LeftSplitContainer.Panel1.ResumeLayout(False)
        Me.LeftSplitContainer.Panel2.ResumeLayout(False)
        Me.LeftSplitContainer.ResumeLayout(False)
        Me.RightSplitContainer.Panel1.ResumeLayout(False)
        Me.RightSplitContainer.Panel2.ResumeLayout(False)
        Me.RightSplitContainer.ResumeLayout(False)
        CType(Me.PictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CloseButton As System.Windows.Forms.Button
    Friend WithEvents EndTurnButton As System.Windows.Forms.Button
    Friend WithEvents ResetButton As System.Windows.Forms.Button
    Friend WithEvents M As System.Windows.Forms.Label
    Friend WithEvents J As System.Windows.Forms.Label
    Friend WithEvents A As System.Windows.Forms.Label
    Friend WithEvents C As System.Windows.Forms.Label
    Friend WithEvents MainGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BodySplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents LeftSplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents RightSplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents MTextBox As System.Windows.Forms.TextBox
    Friend WithEvents JTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ATextBox As System.Windows.Forms.TextBox
    Friend WithEvents CTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MListBox As System.Windows.Forms.ListBox
    Friend WithEvents JListBox As System.Windows.Forms.ListBox
    Friend WithEvents AListBox As System.Windows.Forms.ListBox
    Friend WithEvents CListBox As System.Windows.Forms.ListBox
    Friend WithEvents TurnCounter As System.Windows.Forms.Label
    Friend WithEvents MCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents JCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ACheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents MTurnLabel As System.Windows.Forms.Label
    Friend WithEvents JTurnLabel As System.Windows.Forms.Label
    Friend WithEvents ATurnLabel As System.Windows.Forms.Label
    Friend WithEvents CTurnLabel As System.Windows.Forms.Label
    Friend WithEvents TurnCounterLabel As System.Windows.Forms.Label
    Friend WithEvents PhaseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PhaseLabel As System.Windows.Forms.Label
    Friend WithEvents CheckPhaseButton As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents SaveButton As System.Windows.Forms.Button
    Friend WithEvents SaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FunReqsCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents MOnGoingLabel As System.Windows.Forms.Label
    Friend WithEvents JOnGoingLabel As System.Windows.Forms.Label
    Friend WithEvents AOnGoingLabel As System.Windows.Forms.Label
    Friend WithEvents COnGoingLabel As System.Windows.Forms.Label
    Friend WithEvents PictureBox As System.Windows.Forms.PictureBox

End Class
